# final-miscosas

Repositorio de inicio de la práctica final. Recuerda realizar una derivación (fork) de este repositorio y hacer múltiples commits al realizar tu práctica.

# Entrega practica

## Datos

* Nombre: Juan Ignacio Magaz Santiago
* Titulación: Ingeniería en Tecnologías de la Telecomunicación
* Despliegue (url): juanchomagaz.pythonanywhere.com
* Video básico (url): https://www.youtube.com/watch?v=8dC8pxvl0zo&feature=youtu.be
* Video parte opcional (url): https://www.youtube.com/watch?v=Ao7zdV8xaTo&feature=youtu.be

## Cuenta Admin Site

* usuario/contraseña administrator/admin1234

## Cuentas usuarios

* usuario/contraseña pepe/pepeypaco
* usuario/contraseña juan/juanitos

## Resumen parte obligatoria

En todas las páginas se muestran los formularios para introducir las ID de Youtube y Flickr, la lista de alimentadores, los 5 últimos items votados y el top 10 de items de la página.

## Lista partes opcionales

* Nombre parte: Favicon.
* Nombre parte: Imagen en los comentarios mediante URL.
* Nombre parte: Bootstrap.
