#!/usr/bin/python3

# Simple XML parser for YouTube XML channels
# Jesus M. Gonzalez-Barahona <jgb @ gsyc.es> 2020
# SARO and SAT subjects (Universidad Rey Juan Carlos)
#
# Example of XML document for a YouTube channel:
# https://www.youtube.com/feeds/videos.xml?channel_id=UC300utwSVAYOoRLEqmsprfg

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string
from .models import Alimentador, Item

class FlickerHandler(ContentHandler):
    """Class to handle events fired by the SAX parser

    Fills in self.videos with title, link and id for videos
    in a YT channel XML document.
    """

    def __init__ (self):
        """Initialization of variables for the parser
        * inEntry: within <entry>
        * inContent: reading interesting target content (leaf strings)
        * content: target content being readed
        * title: title of the current entry
        * id: id of the current entry
        * link: link of the current entry
        * videos: list of videos (<entry> elements) in the channel,
            each video is a dictionary (title, link, id)
        """
        self.inEntry = False
        self.inContent = False
        self.content = ""
        self.id = ""
        self.nombretiqueta = ""
        self.linketiqueta = ""
        self.nitems = 0

        self.idfoto = ""
        self.titulo = ""
        self.linkfoto = ""
        self.picture = ""
        self.fotos = []

    def startElement (self, name, attrs):
        if name == 'entry':
            self.inEntry = True
        elif self.inEntry:
            if name == 'title':
                self.inContent = True
            elif name == 'id':
                self.inContent = True
            elif name == 'link' and attrs.get('rel') == 'alternate':
                self.linkfoto = attrs.get('href')
            elif name == 'link' and attrs.get('rel') == 'enclosure':
                self.picture = attrs.get('href')
        elif not self.inEntry:
            if name == 'link' and attrs.get('rel') == 'alternate':
                self.linketiqueta = attrs.get('href')
            elif name == 'id':
                self.inContent = True

    def endElement (self, name):
        global fotos

        if name == 'entry':
            self.inEntry = False
            try:
                etiqueta = Alimentador.objects.get(id = self.id.split("/")[-1])
            except Alimentador.DoesNotExist:
                etiqueta = Alimentador(id = self.id.split("/")[-1], nombre = self.id.split("/")[-1],
                enlace = self.linketiqueta, nitems = 0, puntuacion = 0, type = 'flickr')
            etiqueta.save()

            try:
                foto = Item.objects.get(iditem = self.idfoto.split("/")[-1])
            except:
                foto = Item(alimentador = etiqueta, iditem = self.idfoto.split("/")[-1], titulo = self.titulo, enlace = self.linkfoto,
                        descripcion = '', picture = self.picture, votpos = 0, votneg = 0)
                foto.save()
                self.fotos.append(foto)
                etiqueta.nitems = etiqueta.nitems + 1
                etiqueta.save()

        elif self.inEntry:
            if name == 'title':
                self.titulo = self.content
            elif name == 'id':
                self.idfoto = self.content
            self.content = ""
            self.inContent = False
        elif not self.inEntry:
            if name == 'id':
                self.id = self.content
            self.content = ""
            self.inContent = False

    def characters (self, chars):
        if self.inContent:
            self.content = self.content + chars

class FlickerTag:
    """Class to get videos in a YouTube channel.

    Extracts video links and titles from the XML document for a YT channel.
    The list of videos found can be retrieved lated by calling videos()
    """

    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = FlickerHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)
