from django.contrib import admin
from .models import Alimentador, Item, Vote, Comment, UserProfile
# Register your models here.
admin.site.register(Alimentador)
admin.site.register(Item)
admin.site.register(Vote)
admin.site.register(Comment)
admin.site.register(UserProfile)
