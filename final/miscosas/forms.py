from django import forms
from .models import UserProfile

class UploadImageForm(forms.ModelForm):

    class Meta:
        model = UserProfile
        fields = ('foto',)
