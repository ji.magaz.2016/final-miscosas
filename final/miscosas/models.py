from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
# Create your models here.
class Alimentador(models.Model):
    id = models.CharField(max_length=50, primary_key=True)#primary_key=True para poder sustituir el id por defecto
    nombre = models.CharField(max_length=256, null=True)
    enlace = models.CharField(max_length=256, null=True)
    nitems = models.IntegerField(default=0)
    puntuacion = models.IntegerField(default=0)
    selected = models.BooleanField(default = True)
    type = models.CharField(max_length=7, null=True)
    def __str__(self):
        return self.nombre

class Item(models.Model):
    alimentador = models.ForeignKey(Alimentador, on_delete=models.CASCADE)
    iditem = models.CharField(max_length=50, null=True)
    titulo = models.CharField(max_length=256, null=True)
    enlace = models.URLField(max_length=256, null=True)
    descripcion = models.CharField(max_length=256, null=True)
    picture = models.URLField(null=True)
    votpos = models.IntegerField(default=0)
    votneg = models.IntegerField(default=0)
    puntuacion = models.IntegerField(null=True)
    def __str__(self):
        return self.titulo

class Vote(models.Model):
    voto = models.IntegerField(default=0)
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    item = models.ForeignKey(Item,on_delete=models.CASCADE)
    def __str__(self):
        return 'Ha votado ' + self.user.username + ' en el item: ' + self.item.titulo

class Comment(models.Model):
    comment = models.CharField(max_length=250, null = True)
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    item = models.ForeignKey(Item,on_delete=models.CASCADE)
    title = models.CharField(max_length = 50, null = True)
    date = models.DateTimeField(default=timezone.now)
    fotourl = models.URLField(null=True)
    def __str__(self):
        return 'Ha comentado ' + self.user.username + ' en el item: ' + self.item.titulo + ': ' + self.comment

class UserProfile(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    foto = models.ImageField(upload_to = '', default = 'default.jpg')
    darkmode = models.BooleanField(default = False)
    tamanholetra = models.CharField(max_length=10, null=True)
    def __str__(self):
        return self.user.username
