from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from django.contrib.auth import logout
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import authenticate
from django.contrib.auth import login as do_login
from django.template.loader import get_template
from django.core import serializers
from django.template import RequestContext
import urllib.request
from .models import Alimentador, Item, Vote, Comment, UserProfile, User
from .ytparser import YTChannel
from .flickrparser import FlickerTag
from .forms import UploadImageForm

# Create your views here.
def login(request):
    user = request.user
    usuarios = UserProfile.objects.all()
    try:
        imagen = UserProfile.objects.get(user = user)
        usuario = UserProfile.objects.get(user = user)
        estilousuario = UserProfile.objects.get(user = request.user)
    except:
        imagen = UserProfile.objects.none()
        usuario = UserProfile.objects.none()
        estilousuario = UserProfile.objects.none()
    # Creamos el formulario de autenticación vacío
    form = AuthenticationForm()
    if request.method == "POST":
        # Añadimos los datos recibidos al formulario
        form = AuthenticationForm(data=request.POST)
        # Si el formulario es válido...
        if form.is_valid():
            # Recuperamos las credenciales validadas
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            # Verificamos las credenciales del usuario
            user = authenticate(username=username, password=password)

            # Si existe un usuario con ese nombre y contraseña
            if user is not None:
                # Hacemos el login manualmente
                do_login(request, user)
                # Y le redireccionamos a la portada
                url = '/' + user.username
                return redirect(url)
    context = {'form': form, 'imagen': imagen, 'usuarios': usuarios, 'usuario': usuario, 'estilousuario': estilousuario}
    return render(request, "miscosas/index.html", context)

def contextos(request):
    form = AuthenticationForm()
    alimentadores = Alimentador.objects.all()
    usuarios = UserProfile.objects.all()
    topitems = Item.objects.order_by('-puntuacion')[:10]

    try:
        user = request.user
        votos = Vote.objects.filter(user = user)
        uservotes = Vote.objects.filter(user = user)
        imagen = UserProfile.objects.get(user = user)
        usuario = UserProfile.objects.get(user = user)
        useritems = Vote.objects.filter(user = user)[:5]
        estilousuario = UserProfile.objects.get(user = user)
    except:
        votos = Vote.objects.none()
        uservotes = Vote.objects.none()
        imagen = UserProfile.objects.none()
        usuario = UserProfile.objects.none()
        useritems = Vote.objects.none()
        estilousuario = UserProfile.objects.none()

    votelist = []
    for item in uservotes:
        votelist.append(item.item)

    context = {'form': form, 'alimentadores': alimentadores, 'usuarios': usuarios,
    'topitems': topitems, 'votos': votos, 'votelist': votelist, 'imagen': imagen,
    'usuario': usuario,  'useritems': useritems, 'estilousuario': estilousuario}

    return context

def indexxml(user):
    template = get_template("miscosas/index.xml")
    alimentadores = Alimentador.objects.filter(selected=True)
    items = Item.objects.order_by('-puntuacion')[:10]
    c = RequestContext(user, {"alimentadores" : alimentadores, "items" : items}).flatten()
    if user.is_authenticated:
        lastvoted = Vote.objects.filter(user=user)[:5]
        c = RequestContext(user, {"user": user, "alimentadores": alimentadores, "items": items, "voteditems": lastvoted}).flatten()
    respuesta = template.render(c)
    return HttpResponse(respuesta, status=200, content_type="application/xml")

def indexjson(user):
    all = [*Alimentador.objects.filter(selected=True), *Item.objects.order_by('-puntuacion')[:10]]
    json = serializers.serialize('json', all)
    if user.is_authenticated:
        all = [*Alimentador.objects.filter(selected=True), *Item.objects.order_by('-puntuacion')[:10], *Vote.objects.filter(user=user)[:5]]
        json = serializers.serialize('json', all)
    return json

def index(request):
    context1 = contextos(request)
    if request.method == "GET":
        formato = request.GET.get('format')
        if formato:
            if formato == 'xml':
                return HttpResponse(indexxml(request.user), status=200, content_type="application/xml")
            else:
                return HttpResponse(indexjson(request.user), status=200, content_type="application/json")

    context = {'page': 'Inicio'}
    context.update(context1)
    return render(request, "miscosas/format.html", context)

def parse(request, id, alim):
    if alim == 'yt':
        if id != '':
                xml = "https://www.youtube.com/feeds/videos.xml?channel_id=" + id
                xmlStream = urllib.request.urlopen(xml)
                channel = YTChannel(xmlStream)
    elif alim == 'flickr':
        if id != '':
                xml = "https://www.flickr.com/services/feeds/photos_public.gne?tags=" + id
                xmlStream = urllib.request.urlopen(xml)
                channel = FlickerTag(xmlStream)

def alimentadores(request):
    context1 = contextos(request)
    context = {'page': 'Alimentadores'}
    context.update(context1)
    return render(request, "miscosas/alimentadores.html", context)

def crearalimentador(request):
    if request.method == 'POST':
        id = request.POST['id']
        action = request.POST['Alimentador']
        try:
            parsertype = request.POST['parsertype']
        except:
            parsertype = ''

        if action == 'Canal':
            parse(request, id, 'yt')
        elif action == 'Etiqueta':
            parse(request, id, 'flickr')
        elif action == 'Elegir':
            if parsertype == 'youtube':
                parse(request, id, 'yt')
            elif parsertype == 'flickr':
                parse(request, id, 'flickr')

        alimentador = Alimentador.objects.get(id=id)
        alimentador.selected = True
        alimentador.save()

    url = '/alimentador/' + alimentador.id
    return redirect(url)

def alimentador(request, idalim):
    context1 = contextos(request)
    if request.method == 'POST':
        id = request.POST['id']
        alimentador = Alimentador.objects.get(id=id)
        alimentador.selected = True
        alimentador.save()
    alimentador = Alimentador.objects.get(id=idalim)
    items = Item.objects.filter(alimentador = alimentador)
    context = {'alimentador': alimentador, 'items': items}
    context.update(context1)
    return render(request, "miscosas/alim.html", context)

def delete(request):
    if request.method == 'POST':
        id = request.POST['id']
        alimentador = Alimentador.objects.get(id=id)
        alimentador.selected = False
        alimentador.save()

        return redirect('/alimentadores')

def video(request, idvideo):
    user = request.user
    item = Item.objects.get(iditem = idvideo)
    context1 = contextos(request)
    try:
        voto = Vote.objects.filter(user = user).get(item = item)
    except:
        voto = Vote.objects.none()
    video = get_object_or_404(Item, iditem = idvideo)
    comments = Comment.objects.filter(item = video)
    vidempotrado = "https://www.youtube.com/embed/" + idvideo

    context = {'voto': voto, 'video': video, 'comments': comments,
                                        'vidempotrado': vidempotrado}
    context.update(context1)
    return render(request, 'miscosas/video.html', context)

def foto(request, idfoto):
    user = request.user
    item = Item.objects.get(iditem = idfoto)
    context1 = contextos(request)
    try:
        voto = Vote.objects.filter(user = user).get(item = item)
    except:
        voto = Vote.objects.none()
    foto = get_object_or_404(Item, iditem = idfoto)
    comments = Comment.objects.filter(item = foto)
    context = {'voto': voto, 'foto': foto, 'comments': comments}
    context.update(context1)
    return render(request, 'miscosas/foto.html', context)

def vote(request, iditem):
    if request.method == 'POST':
        item = Item.objects.get(iditem = iditem)
        vote = request.POST['vote']
        user = request.user

        try:
            votoant = Vote.objects.filter(user = user).get(item = item)
            if votoant.voto == 1 and vote == 'dislike':
                item.votpos = item.votpos -1
                item.votneg = item.votneg + 1
                item.puntuacion = item.votpos - item.votneg
                item.save()
                votoant.voto = -1
                votoant.save()
                item.alimentador.puntuacion = item.alimentador.puntuacion - 2
                item.alimentador.save()
            if votoant.voto == -1 and vote == 'like':
                item.votneg = item.votneg -1
                item.votpos = item.votpos + 1
                item.puntuacion = item.votpos - item.votneg
                item.save()
                votoant.voto = 1
                votoant.save()
                item.alimentador.puntuacion = item.alimentador.puntuacion + 2
                item.alimentador.save()
            if item.descripcion:
                url = '/youtube/' + iditem
            elif item.picture:
                url = '/flickr/' + iditem
            return redirect(url)

        except:
            if vote == 'like':
                item.votpos = item.votpos + 1
                item.puntuacion = item.votpos - item.votneg
                item.save()
                voted = Vote(user = user, item = item, voto = 1)
                voted.save()
                item.alimentador.puntuacion = item.alimentador.puntuacion + 1
                item.alimentador.save()
            else:
                item.votneg = item.votneg + 1
                item.puntuacion = item.votpos - item.votneg
                item.save()
                voted = Vote(user = user, item = item, voto = -1)
                voted.save()
                item.alimentador.puntuacion = item.alimentador.puntuacion - 1
                item.alimentador.save()
            if item.descripcion:
                url = '/youtube/' + iditem
            elif item.picture:
                url = '/flickr/' + iditem
            return redirect(url)

def comment(request, iditem):
    if request.method == 'POST':
        item = Item.objects.get(iditem = iditem)
        title = request.POST['Title']
        comment = request.POST['Comment']
        foto = request.POST['Foto']
        user = request.user

        commentfin = Comment(user = user, item = item, title = title, comment = comment, fotourl = foto)
        commentfin.save()

        if item.descripcion:
            url = '/youtube/' + iditem
        elif item.picture:
            url = '/flickr/' + iditem

    return redirect(url)

def usuarios(request):
    context1 = contextos(request)
    context = {'page': 'Usuarios'}
    context.update(context1)
    return render(request, "miscosas/users.html", context)

def loggedIn(request, username):
    formfoto = UploadImageForm()
    context1 = contextos(request)
    user = User.objects.get(username = username)
    comentarios = Comment.objects.filter(user = user)
    votos = Vote.objects.filter(user = user)

    try:
        usuario = UserProfile.objects.get(user = user)
    except:
        usuario = UserProfile(user = user)
        usuario.save()

    try:
        imagen = UserProfile.objects.get(user = request.user)
    except:
        if request.user.is_authenticated:
            imagen = UserProfile(user = request.user)
            imagen.save()
        else:
            imagen = UserProfile.objects.none()

    if request.method == 'POST':
        formfoto = UploadImageForm(request.FILES)
        imagen.foto = request.FILES.get('foto')
        imagen.save()

    context = {'formfoto': formfoto, 'comentarios': comentarios, 'votos': votos,
                    'usuario': usuario, 'imagen': imagen, 'page': 'Mi Perfil'}
    context1.update(context)
    return render(request, "miscosas/user.html", context1)

def darkmode(request):
    usuario = UserProfile.objects.get(user = request.user)
    if request.method == 'POST':
        action = request.POST['Cambiar']
        if action == 'Darkmode':
            usuario.darkmode = True
        elif action == 'Lightmode':
            usuario.darkmode = False
        usuario.save()
    url = '/' + usuario.user.username
    return redirect(url)

def tamletra(request):
    usuario = UserProfile.objects.get(user = request.user)
    if request.method == 'POST':
        action = request.POST['Letra']
        if action == 'Pequeño':
            usuario.tamanholetra = 'Pequeño'
        elif action == 'Mediano':
            usuario.tamanholetra = 'Mediano'
        elif action == 'Grande':
            usuario.tamanholetra = 'Grande'
        usuario.save()
    url = '/' + usuario.user.username
    return redirect(url)

def informacion(request):
    context1 = contextos(request)
    context = {'page': 'Informacion'}
    context.update(context1)
    return render(request, "miscosas/info.html", context)

def logout_view(request):
    logout(request)
    return redirect("/")

def register(request):
    context1 = contextos(request)
    # Creamos el formulario de autenticación vacío
    registerform = UserCreationForm()
    if request.method == "POST":
        # Añadimos los datos recibidos al formulario
        registerform = UserCreationForm(data=request.POST)
        # Si el formulario es válido...
        if registerform.is_valid():
            # Creamos la nueva cuenta de usuario
            user = registerform.save()
            # Si el usuario se crea correctamente
            if user is not None:
                # Hacemos el login manualmente
                do_login(request, user)
                # Y le redireccionamos a la portada
                url = '/' + user.username
                return redirect(url)

    # Si llegamos al final renderizamos el formulario

    context = {'registerform': registerform, 'page': 'Registrate'}
    context.update(context1)
    return render(request, "miscosas/register.html", context)
