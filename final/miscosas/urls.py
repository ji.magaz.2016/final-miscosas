"""final URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from . import views
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.index),
    path('logout', views.logout_view),
    path('login', views.login),
    path('register', views.register),
    path('delete', views.delete),
    path('alimentador', views.crearalimentador),
    path('alimentador/<str:idalim>', views.alimentador),
    path('alimentadores', views.alimentadores),
    path('usuarios', views.usuarios),
    path('flickr/<str:idfoto>', views.foto),
    path('youtube/<str:idvideo>', views.video),
    path('alimentador/<str:iditem>/vote', views.vote),
    path('alimentador/<str:iditem>/comment', views.comment),
    path('darkmode', views.darkmode),
    path('tamletra', views.tamletra),
    path('informacion', views.informacion),
    path('<str:username>', views.loggedIn),
]
