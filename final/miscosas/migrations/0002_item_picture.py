# Generated by Django 3.0.3 on 2020-05-25 15:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('miscosas', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='picture',
            field=models.URLField(null=True),
        ),
    ]
