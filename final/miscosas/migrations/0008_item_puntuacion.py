# Generated by Django 3.0.3 on 2020-05-30 18:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('miscosas', '0007_userprofile_tamanholetra'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='puntuacion',
            field=models.IntegerField(null=True),
        ),
    ]
