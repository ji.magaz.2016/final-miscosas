from django.test import TestCase
from .models import Alimentador, Item, UserProfile, User
# Create your tests here.

class AlimentadorTestCase(TestCase):
    def setUp(self):
        self.id="UCMl6IV2MR5W2gg4qHYAqcoQ"
        self.nombre="sergioagueroTV"

    def testGuardarAlimentador(self):
        alimentador = Alimentador(id = self.id, nombre = self.nombre)
        alimentador.save()

class ItemTestCase(TestCase):
    def setUp(self):
        self.id="UCMl6IV2MR5W2gg4qHYAqcoQ"
        self.nombre="sergioagueroTV"
        self.iditem="SRWX98srdJc"
        self.titulo="Drafteados - NBA en español"

    def testGuardarItem(self):
        alimentador = Alimentador(id = self.id, nombre = self.nombre)
        alimentador.save()
        item = Item(alimentador = alimentador, iditem = self.iditem, titulo = self.titulo)
        item.save()

class UserTestCase(TestCase):
    def setUp(self):
        self.user = User(username="Administrator")
        self.usuario = UserProfile(user = self.user, darkmode = True)

    def testUserProfile(self):
        self.assertEquals(self.usuario.darkmode, True)

class ViewsTestCase(TestCase):
    def setUp(self):
        self.alimentador = Alimentador(id = "UCMl6IV2MR5W2gg4qHYAqcoQ", nombre = "sergioagueroTV",
        enlace = "https://www.youtube.com/user/sergioagueroTV", nitems = 2, puntuacion = 1, selected = True, type = "youtube")
        self.alimentador.save()
        self.youtubeitem = Item(alimentador = self.alimentador, iditem = "SRWX98srdJc", titulo = "Drafteados - NBA en español",
        enlace = "https://www.youtube.com/watch?v=QHBdYBJcNUw", descripcion = 'directo', votpos = 2, votneg = 0, puntuacion = 1)
        self.youtubeitem.save()
        self.flickritem = Item(alimentador = self.alimentador, iditem = "49844651528", titulo = "49844651528",
        enlace = "https://www.flickr.com/photos/jolugoma/49844651528/", picture = ' https://live.staticflickr.com/65535/49988888892_2e389f753c_b.jpg', votpos = 2, votneg = 0, puntuacion = 1)
        self.flickritem.save()

    def testIndex(self):
        response = self.client.get('/')
        response2 = self.client.get('/?format=xml')
        response3 = self.client.get('/?format=json')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response2.status_code, 200)
        self.assertEqual(response3.status_code, 200)

    def testLogin(self):
        response = self.client.get('/login')
        self.assertEqual(response.status_code, 200)

    def testRegister(self):
        response = self.client.get('/register')
        self.assertEqual(response.status_code, 200)

    def testAlimentador(self):
        url = '/alimentador/' + self.alimentador.id
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def testAlimentadores(self):
        response = self.client.get('/alimentadores')
        self.assertEqual(response.status_code, 200)

    def testUsuarios(self):
        response = self.client.get('/usuarios')
        self.assertEqual(response.status_code, 200)

    def testFlickrItem(self):
        url = '/flickr/' + self.flickritem.iditem
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def testYoutubeItem(self):
        url = '/youtube/' + self.youtubeitem.iditem
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def testInfo(self):
        response = self.client.get('/informacion')
        self.assertEqual(response.status_code, 200)
